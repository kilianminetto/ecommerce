<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

/**
 * class CategoryTest
 */
class CategoryTest extends TestCase
{
    public function testIfCategoryIsCreated(): void
    {
        $category = new Category();
        $category->setName("category");

        $this->assertEquals("category", $category->getName());
    }
}
